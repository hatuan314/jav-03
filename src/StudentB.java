import java.util.Scanner;

public class StudentB extends Student {
    private double mathPoint;
    private double chemistryPoint;
    private double biologicalPoint;

    public StudentB(int id, String fullName, String address, int prioritize, double mathPoint, double chemistryPoint, double biologicalPoint) {
        super(id, fullName, address, prioritize);
        this.mathPoint = mathPoint;
        this.chemistryPoint = chemistryPoint;
        this.biologicalPoint = biologicalPoint;
    }

    public StudentB() {

    }

    public double getMathPoint() {
        return mathPoint;
    }

    public void setMathPoint(double mathPoint) {
        this.mathPoint = mathPoint;
    }

    public double getChemistryPoint() {
        return chemistryPoint;
    }

    public void setChemistryPoint(double chemistryPoint) {
        this.chemistryPoint = chemistryPoint;
    }

    public double getBiologicalPoint() {
        return biologicalPoint;
    }

    public void setBiologicalPoint(double biologicalPoint) {
        this.biologicalPoint = biologicalPoint;
    }

    @Override
    void setInfo(int index) {
        Scanner scString = new Scanner(System.in);
        Scanner scNum = new Scanner(System.in);
        // Set Id
        setId(index);
        // Set fullname
        System.out.print("Họ và tên: ");
        setFullName(scString.nextLine());
        // Set Address
        System.out.print("Địa chỉ: ");
        setAddress(scString.nextLine());
        // Set Prioritize
        System.out.print("Ưu tiên: ");
        setPrioritize(scNum.nextInt());

        // Set Math Point
        System.out.print("điểm toán:");
        this.mathPoint = scNum.nextDouble();
        // Set Physical Point
        System.out.print("điểm Sinh học:");
        this.biologicalPoint = scNum.nextDouble();
        System.out.print("điểm hóa:");
        this.chemistryPoint = scNum.nextDouble();
    }

    @Override
    void getInfo() {
        System.out.println("Số báo danh:" + super.getId());
        System.out.println("Họ tên:" + super.getFullName());
        System.out.println("Địa chỉ:" + super.getAddress());
        System.out.println("Ưu tiên:" + super.getPrioritize());
        System.out.println("Điểm Toán:" + this.getMathPoint());
        System.out.println("Điểm Sinh học:" + this.biologicalPoint);
        System.out.println("Điểm Hóa:" + this.chemistryPoint);
    }
}
