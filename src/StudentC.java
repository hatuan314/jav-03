import java.util.Scanner;

public class StudentC extends Student {
    private double literaryPoint;
    private double historyPoint;
    private double geographyPoint;

    public StudentC(int id, String fullName, String address, int prioritize, double literaryPoint, double historyPoint, double geographyPoint) {
        super(id, fullName, address, prioritize);
        this.literaryPoint = literaryPoint;
        this.historyPoint = historyPoint;
        this.geographyPoint = geographyPoint;
    }

    public StudentC() {

    }

    public double getLiteraryPoint() {
        return literaryPoint;
    }

    public void setLiteraryPoint(double literaryPoint) {
        this.literaryPoint = literaryPoint;
    }

    public double getHistoryPoint() {
        return historyPoint;
    }

    public void setHistoryPoint(double historyPoint) {
        this.historyPoint = historyPoint;
    }

    public double getGeographyPoint() {
        return geographyPoint;
    }

    public void setGeographyPoint(double geographyPoint) {
        this.geographyPoint = geographyPoint;
    }

    @Override
    void setInfo(int index) {
        Scanner scString = new Scanner(System.in);
        Scanner scNum = new Scanner(System.in);
        // Set Id
        setId(index);
        // Set fullname
        System.out.print("Họ và tên: ");
        setFullName(scString.nextLine());
        // Set Address
        System.out.print("Địa chỉ: ");
        setAddress(scString.nextLine());
        // Set Prioritize
        System.out.print("Ưu tiên: ");
        setPrioritize(scNum.nextInt());

        // Set Math Point
        System.out.print("điểm văn:");
        this.literaryPoint = scNum.nextDouble();
        // Set Physical Point
        System.out.print("điểm sử:");
        this.historyPoint = scNum.nextDouble();
        System.out.print("điểm đại:");
        this.geographyPoint = scNum.nextDouble();
    }

    @Override
    void getInfo() {
        System.out.println("Số báo danh:" + super.getId());
        System.out.println("Họ tên:" + super.getFullName());
        System.out.println("Địa chỉ:" + super.getAddress());
        System.out.println("Ưu tiên:" + super.getPrioritize());
        System.out.println("Điểm Văn:" + this.literaryPoint);
        System.out.println("Điểm Sử" + this.getHistoryPoint());
        System.out.println("Điểm Địa:" + this.geographyPoint);
    }
}
