public abstract class Student {
    private int id;
    private String fullName;
    private String address;
    private int prioritize;

    public Student(int id, String fullName, String address, int prioritize) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.prioritize = prioritize;
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPrioritize() {
        return prioritize;
    }

    public void setPrioritize(int prioritize) {
        this.prioritize = prioritize;
    }

    abstract void setInfo(int index);

    abstract void getInfo();
}
