import java.util.Scanner;

public class HomeworkMain02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Admissions admissions = new Admissions();
        // Nhập thông tin
        admissions.setStudentInfo();
        // Hiển thị thông tin của 1 sinh viên
        int studentId;
        do {
            System.out.println("Nhập mã sinh viên: ");
            studentId = sc.nextInt();
        } while (studentId > 5);
        admissions.searchStudentById(studentId);

    }
}
