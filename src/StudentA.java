import java.util.Scanner;

public class StudentA extends Student {
    private double mathPoint;
    private double physicalPoint;
    private double chemistryPoint;

    public StudentA(int id, String fullName, String address, int prioritize, double mathPoint, double physicalPoint, double chemistryPoint) {
        super(id, fullName, address, prioritize);
        this.mathPoint = mathPoint;
        this.physicalPoint = physicalPoint;
        this.chemistryPoint = chemistryPoint;
    }
    public StudentA(){
        super();
    };

    public double getMathPoint() {
        return mathPoint;
    }

    public void setMathPoint(double mathPoint) {
        this.mathPoint = mathPoint;
    }

    public double getPhysicalPoint() {
        return physicalPoint;
    }

    public void setPhysicalPoint(double physicalPoint) {
        this.physicalPoint = physicalPoint;
    }

    public double getChemistryPoint() {
        return chemistryPoint;
    }

    public void setChemistryPoint(double chemistryPoint) {
        this.chemistryPoint = chemistryPoint;
    }

    @Override
    void setInfo(int index) {
        Scanner scString = new Scanner(System.in);
        Scanner scNum = new Scanner(System.in);
        System.out.println(index);
        // Set Id
        setId(index);
        // Set fullname
        System.out.print("Họ và tên: ");
        setFullName(scString.nextLine());
        // Set Address
        System.out.print("Địa chỉ: ");
        setAddress(scString.nextLine());
        // Set Prioritize
        System.out.print("Ưu tiên: ");
        setPrioritize(scNum.nextInt());

        // Set Math Point
        System.out.print("điểm toán:");
        this.mathPoint = scNum.nextDouble();
        // Set Physical Point
        System.out.print("điểm lý:");
        this.physicalPoint = scNum.nextDouble();
        System.out.print("điểm hóa:");
        this.chemistryPoint = scNum.nextDouble();
    }

    @Override
    void getInfo() {
        System.out.println("Số báo danh:" + super.getId());
        System.out.println("Họ tên:" + super.getFullName());
        System.out.println("Địa chỉ:" + super.getAddress());
        System.out.println("Ưu tiên:" + super.getPrioritize());
        System.out.println("Điểm Toán:" + this.getMathPoint());
        System.out.println("Điểm Lý:" + this.physicalPoint);
        System.out.println("Điểm Hóa:" + this.chemistryPoint);
    }
}
