public class Admissions {
    private StudentA[] studentAsArray = new StudentA[5];
    private StudentB[] studentBsArray = new StudentB[5];
    private StudentC[] studentCsArray = new StudentC[5];

    public void setStudentInfo() {

        for (int index = 0; index < 3; index++) {
            int studentId = index + 1;
            switch (index) {
                case 0:
                    StudentA studentA = new StudentA();
                    studentA.setInfo(studentId);
                    studentAsArray[index] = studentA;
                    break;
                case 1:
                    StudentB studentB = new StudentB();
                    studentB.setInfo(studentId);
                    studentBsArray[index] = studentB;
                    break;
                case 2:
                    StudentC studentC = new StudentC();
                    studentC.setInfo(studentId);
                    studentCsArray[index] = studentC;
                    break;
                case 3:
                    StudentB studentB1 = new StudentB();
                    studentB1.setInfo(studentId);
                    studentBsArray[index] = studentB1;
                    break;
                case 4:
                    StudentA studentA1 = new StudentA();
                    studentA1.setInfo(studentId);
                    studentAsArray[index] = studentA1;
                    break;
            }
        }
    }

    private void getAllStudent() {
        for(int index = 0; index < studentAsArray.length; index ++) {
            if (studentAsArray[index] != null) {
                studentAsArray[index].getInfo();
            }
        }

        for(int index = 0; index < studentBsArray.length; index ++) {
            if (studentBsArray[index] != null) {
                studentBsArray[index].getInfo();
            }
        }

        for(int index = 0; index < studentCsArray.length; index ++) {
            if (studentCsArray[index] != null) {
                studentCsArray[index].getInfo();
            }
        }
    }

    public boolean searchStudentById(int studentId) {
        for (int index = 0; index < studentAsArray.length; index++) {
            if (studentAsArray[index] != null && studentId == studentAsArray[index].getId()) {
                studentAsArray[index].getInfo();
                return true;
            }
        }

        for (int index = 0; index < studentBsArray.length; index++) {
            if (studentBsArray[index] != null && studentId == studentBsArray[index].getId()) {
                studentBsArray[index].getInfo();
                return true;
            }
        }

        for (int index = 0; index < studentCsArray.length; index++) {
            if (studentCsArray[index] != null && studentId == studentCsArray[index].getId()) {
                studentCsArray[index].getInfo();
                return true;
            }
        }

        return false;
    }
}
